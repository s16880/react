import React, { useState } from "react";
import Header from "../shared/Header";
import Table from "../shared/Table";

export default function Main() {
  const usersInitial = [
    { idUser: 1, firstName: "Jan", lastName: "Kowalski" },
    { idUser: 2, firstName: "Andrzej", lastName: "Malewicz" },
    { idUser: 3, firstName: "Anna", lastName: "Andrzejewicz" },
    { idUser: 4, firstName: "Marcin", lastName: "Kwiatkowski" },
    { idUser: 5, firstName: "Michał", lastName: "Kowalski" },
  ];

  const [users, setUsers] = useState(usersInitial);
  const [selectedUser, setSelectedUser] = useState({});

  const addUser = (e) => {
    setUsers([
      ...users,
      {
        idUser: users[users.length - 1].idUser + 1,
        firstName: "AAA",
        lastName: "BBB",
      },
    ]);
  };

  const removeUser = (u) => {
    const filteredUsers = [...users];
    setUsers(
      filteredUsers.filter((user) => user.idUser !== selectedUser.idUser)
    );
  };

  const setCurrentlySelectedUser = (user) => {
    setSelectedUser(user);
  };

  const sortUsers = (col) => {
    let column = "idUser";
    if (col === "First name") {
      column = "firstName";
    }
    if (col === "Last name") {
      column = "lastName";
    }

    const sortedUsers = [...users];
    sortedUsers.sort((a, b) => {
      if (a[column] < b[column]) {
        return -1;
      }
      if (a[column] > b[column]) {
        return 1;
      }
      return 0;
    });
    setUsers(sortedUsers);
  };

  return (
    <>
      <Header />
      <div className="container">
        <br />
        <button type="button" onClick={addUser} className="btn">
          Dodaj użytkownika
        </button>
        <Table
          users={users}
          setSelectedUser={setCurrentlySelectedUser}
          selectedUser={selectedUser}
          sortUsers={sortUsers}
          columnsNames={["Id user", "First name", "Last name"]}
        />
        <button
          type="button"
          onClick={removeUser}
          className="btn"
          style={{ backgroundColor: "red" }}
        >
          Usuń użytkownika
        </button>
      </div>
    </>
  );
}
